"""
XLM Splitter - created by Willem Hendriks

"""
import time
import re
import argparse

"""
Arguments
"""
parser = argparse.ArgumentParser()
parser.add_argument('filename',  help='XML file to split, into multiple chunks containing records')
parser.add_argument('--tag', metavar='t', help='<tag> which defines a records')
parser.add_argument('--groupsize', metavar='g', type=int, help='Size of records inside one chunk', default=10000)
parser.add_argument('--rootelementpostfix', help="String to add to tag, to form a root element name", default="s")
args = parser.parse_args()

XML_FILE = args.filename
#XML_FILE = "sample.xml"

target_tag_open = "<{}>".format( args.tag )
target_tag_close = "</{}>".format( args.tag )
root_element_open = "<{}{}>\n".format( args.tag, args.rootelementpostfix )
root_element_close = "</{}{}>\n".format( args.tag, args.rootelementpostfix )

xml_chunk_counter = 1

with open(XML_FILE, "r") as xml_file_reader:
	xml_chunk_writer = open("chunk_{:03d}".format(xml_chunk_counter), "w")
	xml_chunk_writer.write(root_element_open)

	record_counter = 1
	tag_found = False

	for line in xml_file_reader:

		if target_tag_open in line:
			"""
			We found a <tag> - we can write to a chunk
			"""
			tag_found = True
			record_counter += 1

		if tag_found and target_tag_close in line:
			tag_found = False
			xml_chunk_writer.write(line)

			if record_counter % (1 + args.groupsize) == 0:
				xml_chunk_counter += 1
				record_counter = 1
				xml_chunk_writer.write(root_element_close)
				xml_chunk_writer.close()
				xml_chunk_writer = open("chunk_{:03d}".format(xml_chunk_counter), "w")
				xml_chunk_writer.write(root_element_open)

		if tag_found:
			xml_chunk_writer.write(line)

	xml_chunk_writer.write(root_element_close)
