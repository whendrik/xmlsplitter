# XML splitter

XML Splitter written in python - tool to pre-process large XML files into smaller chunks

Example

To split `large.xml`, which contains many records with tag `<record>` use:

```
python3 xml_splitter.py large.xml --tag "record"
```